import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import {
  Banner,
  Caracteristicas,
  Descripcion,
  Clientes,
  Contacto,
  Planes,
} from "../components"

const IndexPage = () => (
  <Layout ruta="home">
    <SEO title="Inicio" />
    <Banner />
    <Caracteristicas />
    <Descripcion />
    <Planes />
    <Clientes />
    <Contacto />
  </Layout>
)

export default IndexPage
