import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { DescLicencias, Contacto, Licencia } from "../components"

const IndexPage = () => (
  <Layout ruta="licencias">
    <SEO title="Licencias" />
    <Licencia />
    <DescLicencias />
    <Contacto licencias />
  </Layout>
)

export default IndexPage
