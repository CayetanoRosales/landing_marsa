import React from "react"
import {
  BannerComponent,
  ContainerBanner,
  VisorVideo,
  ContainerTitulo,
  ContainerVideo,
} from "../styles/components"

export default () => {
  return (
    <BannerComponent>
      <ContainerBanner>
        <ContainerTitulo
          data-sal="zoom-in"
          data-sal-delay="200"
          data-sal-easing="ease"
          data-sal-duration="300"
        >
          <h1>¡Cambia tu vida!</h1>
          <p>Aprende a disfrutar la libertad de conducir con seguridad.</p>
        </ContainerTitulo>
        <ContainerVideo>
          <VisorVideo>
            <iframe
              iframe
              title="Introducción a Marsa"
              src="https://player.vimeo.com/video/73625505?badge=0&amp;byline=0&amp;title=0&amp;portrait=0"
              width="100%"
              height="290"
              frameBorder="0"
              webkitallowfullscreen
              mozallowfullscreen
              allowFullScreen
            ></iframe>
          </VisorVideo>
        </ContainerVideo>
      </ContainerBanner>
    </BannerComponent>
  )
}
