import styled from "styled-components"
import { colors } from "../../styles/components"

export const Container = styled.section`
  padding: 70px 5%;
  padding-bottom: 100px;
  position: relative;
`

export const ImgVehiculo = styled.div`
  border-radius: 30px;
  width: 360px;
  max-height: 220px;
  margin: 20px;
  box-shadow: 0px 5px 20px #00000066;
  position: relative;

  overflow: hidden;
  height: calc(220 / 340 * 110%);
  picture > img {
    height: auto !important;
  }
  > img {
    height: auto !important;
  }
`
export const VehiculosWrapper = styled.section`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-top: 3rem;
`

export const DotsSuperior = styled.div`
  position: absolute;
  width: 35vw;
  right: 0;
  top: 100px;
  border-top-left-radius: 60px;
  border-bottom-left-radius: 60px;
  overflow: hidden;
`

export const DotsInferior = styled.div`
  position: absolute;
  width: 50vw;
  left: 0;
  bottom: 90px;
  border-top-right-radius: 60px;
  border-bottom-right-radius: 60px;
  overflow: hidden;
`
export const TarjetasWrapper = styled.div`
  margin-top: 50px;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`
export const ContenedorWrapper = styled.div`
  height: 100px;
  display: flex;
  justify-content: center;
`

export const CardWrapper = styled.div`
  position: relative;
  flex: 1;
  color: ${props => (props.resaltar ? "white" : "#11257E")};
  background-color: ${props => (props.resaltar ? "#11257E" : "white")};
  box-shadow: 0px 2px 20px #221f1f33;
  border-radius: 28px;
  padding: 30px;
  display: flex;
  position: relative;
  flex-direction: column;
  min-width: 290px;
  margin: 15px;
  & h5 {
    text-align: center;
    font-size: 1.4rem;
  }

  @media (min-width: 1200px) {
    transform: ${props => (props.resaltar ? "translateY(-20px)" : "none")};
    max-width: 340px;
  }
`

export const IconoWrapper = styled.div`
  position: absolute;
  top: 40px;
  right: 20px;
  height: 50px;
  width: 70px;

  img {
    margin-bottom: 0;
    border-radius: 5px;
  }
`
export const BotonCard = styled.button`
  background-color: ${colors.celeste};
  border-radius: 16px;
  box-shadow: 0px 5px 20px #0000005d;
  color: white;
  text-align: center;
  border: 1px solid ${colors.celeste};
  height: 30px;
  padding: 0px 15px;
  font-size: 0.95rem;
  transition: transform 0.3s ease;
  cursor: pointer;
  &:hover {
    transform: scale(1.05);
  }
  &:active {
    transform: scale(0.98);
  }
`
export const BotonWrapper = styled.div`
  width: 100%;
  position: absolute;
  right: 0;
  left: 0;
  bottom: -15px;
  display: flex;
  justify-content: center;
`
export const CursoLabel = styled.span`
  font-size: 15px;
  color: ${props => (props.resaltar ? "white" : colors.grisOscuro)};
`

export const Titulo = styled.h5`
  font-size: 24px;
  text-align: left !important;
  margin-bottom: 8px;
  color: ${props => (props.resaltar ? "white" : "#11257E")};
`

export const PrecioWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  flex-grow: 1;
  align-items: flex-end;
`

export const Precio = styled.span`
  font-size: 24px;
  font-weight: bold;
`

export const PrecioTexto = styled.span`
  font-size: 16px;
`

export const Descripcion = styled.p`
  margin-left: 0;
  text-align: left !important;
`

export const Caracteristicas = styled.ul`
  list-style: none;
  margin-left: 0;
`

export const ItemCaracteristica = styled.li`
  margin-bottom: 9px;
  padding-left: 28px;
  position: relative;

  &:before {
    content: "✓";
    position: absolute;
    left: 0;
  }
`
