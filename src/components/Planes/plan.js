import React from "react"
import {
  CardWrapper,
  ItemCaracteristica,
  Titulo,
  CursoLabel,
  PrecioWrapper,
  Precio,
  PrecioTexto,
  BotonWrapper,
  BotonCard,
  Caracteristicas,
  Descripcion,
  IconoWrapper,
} from "./styles"

const Card = ({
  nombre,
  icono,
  precio,
  precioTexto,
  descripcion,
  caracteristicas,
  ctaContactenos,
  mensajeWhatsapp,
  resaltar,
}) => {
  const handleContacto = () => {
    window.open(
      `https://api.whatsapp.com/send/?phone=+50254031403&text=${mensajeWhatsapp}&app_absent=0`,
      "_blank"
    )
  }

  return (
    <CardWrapper resaltar={resaltar}>
      <CursoLabel resaltar={resaltar}>Curso</CursoLabel>
      <Titulo resaltar={resaltar}>{nombre}</Titulo>
      <Descripcion>{descripcion}</Descripcion>
      {icono && (
        <IconoWrapper>
          <img src={icono.url} alt={icono.fileName} />
        </IconoWrapper>
      )}
      <Caracteristicas>
        {caracteristicas.map(item => (
          <ItemCaracteristica key={item}>{item}</ItemCaracteristica>
        ))}
      </Caracteristicas>
      {precio && (
        <PrecioWrapper>
          <Precio>{precio}</Precio>
          {precioTexto && <PrecioTexto>{precioTexto}</PrecioTexto>}
        </PrecioWrapper>
      )}
      {ctaContactenos && (
        <BotonWrapper>
          <BotonCard onClick={handleContacto} aria-label="Cursos virtuales">
            {ctaContactenos}
          </BotonCard>
        </BotonWrapper>
      )}
    </CardWrapper>
  )
}

export default Card
