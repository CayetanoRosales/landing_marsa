import React from "react"
import { TituloSeccionContainer } from "../../styles/components"
import {
  Container,
  VehiculosWrapper,
  DotsSuperior,
  DotsInferior,
} from "./styles"
import Plan from "./plan"

const data = [
  {
    nombre: "Marsa Tu",
    icono: null,
    precio: "Q125",
    precioTexto: "/hora",
    descripcion: null,
    caracteristicas: [
      "Evaluación de conocimientos previos",
      "8 horas de teoría de seguridad vial en línea",
      "Pista privada de entrenamiento",
      "Simulador de conducción",
      "Vehículos con doble control de freno",
      "Instructoras e instructores experimentados",
      "Apoyo emocional con especialistas",
    ],
    ctaContactenos: null,
    mensajeWhatsapp: null,
  },
  {
    nombre: "Marsa Plus",
    icono: null,
    precio: "Q3,080",
    precioTexto: null,
    descripcion: null,
    caracteristicas: [
      "28 horas prácticas",
      "8 horas de teoría de seguridad vial en línea",
      "Pista privada de entrenamiento",
      "Simulador de conducción ",
      "Vehículos con doble control de freno",
      "Instructoras e instructores experimentados",
      "Apoyo emocional con especialistas",
      "Capacitación teórica y práctica para licencia de conducir",
    ],
    ctaContactenos: null,
    mensajeWhatsapp: null,
  },
  {
    nombre: "Marsa Pro",
    icono: {
      fileName: "testimonial-6.png",
      url:
        "https://firebasestorage.googleapis.com/v0/b/gag-beae1.appspot.com/o/marsa%2Flicencia.jpeg?alt=media&token=b6ec51a9-7ea4-4ed7-9796-0a2ba6b0d3c2",
    },
    precio: "Q4,420",
    precioTexto: null,
    descripcion: null,
    caracteristicas: [
      "32 horas prácticas",
      "8 horas de teoría de seguridad vial en línea",
      "Pista privada de entrenamiento",
      "Simulador de conducción ",
      "Vehículos con doble control de freno",
      "Instructoras e instructores experimentados",
      "Apoyo emocional con especialistas",
      "Capacitación teórica y práctica para licencia",
      "Manual teórico impreso",
      "Licencia de conducir",
      "Examen de vista",
      "1 año de vigencia para licencia",
    ],
    ctaContactenos: null,
    mensajeWhatsapp: null,
    resaltar: true,
  },
  {
    nombre: "Marsa Enterprise",
    icono: null,
    precio: null,
    precioTexto: null,
    descripcion: "Para instituciones y empresas",
    caracteristicas: [
      "Auditorías de seguridad vial",
      "Seguridad vial laboral",
      "Cursos para pilotos de transporte de activos y/o pasajeros",
      "Diagnóstico a pilotos aspirantes a puestos de trabajo",
      "Certificación para flotas de motocicletas y vehículos de trabajo",
      "Certificación y trámite de licencias de conducir para flotas",
      "Cursos según las necesidades de seguridad vial de las institución o empresa",
    ],
    ctaContactenos: "Contáctenos",
    mensajeWhatsapp:
      "Hola buenas. Deseo información del curso Marsa enterprise",
  },
]
const Cursos = () => {
  return (
    <Container>
      <DotsSuperior>
        <div className="patter-dot" />
      </DotsSuperior>
      <DotsInferior>
        <div className="patter-dot" />
      </DotsInferior>
      <TituloSeccionContainer>
        <h4>¡Sé libre e independiente!</h4>
        <p>
          Puedes lograr tus sueños, el primer paso es elegir el curso que te
          cambiará la vida.
        </p>
      </TituloSeccionContainer>
      <VehiculosWrapper>
        {data.map((plan, index) => (
          <Plan key={`${plan.nombre}-${index}`} {...plan} />
        ))}
      </VehiculosWrapper>
    </Container>
  )
}

export default Cursos
