/* eslint-disable react/jsx-no-target-blank */
/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React, { useCallback, useEffect, useState } from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Footer from "./footer"
import Img from "gatsby-image"

import Header from "./header"
import { elastic as Menu } from "react-burger-menu"
import { BotonMessenger } from "../styles/components"
import "./layout.css"

const facebookAppId = "2826184661039898"
const Layout = ({ children, ruta }) => {
  const [transparente, setTransparente] = useState(true)
  const [menuOpen, setMenuOpen] = useState(false)
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
      allImageSharp {
        edges {
          node {
            fixed(width: 50) {
              ...GatsbyImageSharpFixed_withWebp_noBase64
              originalName
            }
          }
        }
      }
    }
  `)
  const onScroll = useCallback(() => {
    const currentScrollY = window.scrollY
    const alturavp = window.innerHeight * 0.2

    if (currentScrollY > alturavp) {
      setTransparente(false)
    } else if (currentScrollY < alturavp) {
      setTransparente(true)
    }
  }, [])

  useEffect(() => {
    window.addEventListener("scroll", onScroll)
    window.fbAsyncInit = function() {
      window.FB.init({
        xfbml            : true,
        version          : 'v5.0'
      });
    };
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    return () => window.removeEventListener("scroll", onScroll)
  }, [])

  const image = data.allImageSharp.edges.find(edge => {
    return edge.node.fixed.originalName === "messenger.png"
  })

  if (!image) {
    return null
  }
  return (
    <>
      <Menu
        width={280}
        isOpen={menuOpen}
        onClose={() => setMenuOpen(false)}
        right
        disableAutoFocus
        customBurgerIcon={false}
      >
        <a id="home" className="menu-item" href="/">
          Licencias
        </a>
        <a id="contact" className="menu-item" href="/contact">
          Inscribirme
        </a>
      </Menu>

      <Header
        setMenuOpen={setMenuOpen}
        siteTitle={data.site.siteMetadata?.title || `Title`}
        transparente={transparente}
        ruta={ruta}
      />
      <main>{children}</main>
      <Footer />
      
      <BotonMessenger>
        <a href="https://m.me/escuelamarsa92" target="_blank">
          <Img fixed={image.node.fixed} />
        </a>
      </BotonMessenger>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
