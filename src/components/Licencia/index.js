import React from 'react'
import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import { LicenciasWrapper, ImagenWrapper, DotsSuperior } from './styles'

export default () => {
    return (
        <StaticQuery 
            query={graphql`
            query {
              allImageSharp {
                edges {
                  node {
                    fluid(maxHeight: 1000) {
                      aspectRatio
                      ...GatsbyImageSharpFluid
                      originalName
                    }
                  }
                }
              }
            }
          `}
          render={data => {
            const image = data.allImageSharp.edges.find(edge => {
                return edge.node.fluid.originalName === "licencia.png"
            })
            if (!image) {
              return null
            }
            return (
              <LicenciasWrapper>
                  <DotsSuperior>
                    <div className="patter-dot" />
                  </DotsSuperior>
                  <h3 className="text-center gris5">Más que un documento...</h3>
                  <h5 className="text-center gris5">Una gran responsabilidad</h5>
                  <ImagenWrapper>
                    <Img
                        fluid={image.node.fluid}
                        imgStyle={{
                        height: "100%",
                        width: "100%",
                        }}
                    />
                  </ImagenWrapper>
              </LicenciasWrapper>
            )
          }}
        />
    );
}
