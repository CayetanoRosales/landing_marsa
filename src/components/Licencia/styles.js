import styled from "styled-components"
import { deviceMax } from '../../styles'

export const LicenciasWrapper = styled.section`
    margin-top: 110px;
    position: relative;
    padding: 10px;
    height: 80vh;
    color: #707071;
    & h3 {
        text-align: center;
        font-size: 2.5rem;
    }
    & h5 {
        text-align: center;
        font-size: 1.9rem;
        
    }
    @media ${deviceMax.mobileL} {
        height: 70vh;
    }
`;

export const ImagenWrapper = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    width: 50vw;
    & img {
        margin: 0;
    }
    @media ${deviceMax.laptop} {
        width: 90vw;
    }
`

export const DotsSuperior = styled.div`
    position: absolute;
    width: 25vw;
    left: 0;
    top: 100px;
    border-top-right-radius: 60px;
    border-bottom-right-radius: 60px;
    overflow: hidden;
`;