import React from "react"
import { Container } from "../../styles/components"
import {
  AgenciaWrapper,
  ContactoWrapper,
  WhatsappWrapper,
  DireccionesWrapper,
  DotsContacto,
} from "./styles"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faMapMarkerAlt, faPhone } from "@fortawesome/free-solid-svg-icons"
import CardContacto from "./CardContacto"

const Contacto = ({ licencias=false }) => {
  return (
    <Container>
      <DotsContacto>
        <div className="patter-dot" />
      </DotsContacto>
      <AgenciaWrapper>
        <ContactoWrapper data-sal="slide-up" data-sal-easing="ease">
          <h4>AGENCIAS</h4>
          <DireccionesWrapper>
            <strong>Quetzaltenango</strong>
            <ul>
              <li>
                <div>
                  <div>
                    <FontAwesomeIcon icon={faMapMarkerAlt} /> Diagonal 10,
                    0-164, zona 6, Quetzaltenango
                  </div>
                  <div>
                    <FontAwesomeIcon icon={faPhone} />
                    +502 7765 1518
                  </div>
                </div>
              </li>
              <li>
                <div>
                  <div>
                    <FontAwesomeIcon icon={faMapMarkerAlt} /> 18 Avenida 0-60,
                    zona 3, Quetzaltenango
                  </div>
                  <div>
                    <FontAwesomeIcon icon={faPhone} />
                    +502 7765 1518
                  </div>
                </div>
              </li>
              <li>
                <div>
                  <div>
                    <FontAwesomeIcon icon={faMapMarkerAlt} /> 21 Avenida 3-38,
                    zona 3, Quetzaltenango
                  </div>
                  <div>
                    <FontAwesomeIcon icon={faPhone} />
                    +502 7765 1717
                  </div>
                </div>
              </li>
            </ul>
          </DireccionesWrapper>
          <DireccionesWrapper>
            <strong>San Marcos</strong>
            <ul>
              <li>
                <div>
                  <div>
                    <FontAwesomeIcon icon={faMapMarkerAlt} /> 9na Calle 5-03,
                    zona 1, San Marcos
                  </div>
                  <div>
                    <FontAwesomeIcon icon={faPhone} />
                    +502 5651 5928
                  </div>
                </div>
              </li>
            </ul>
          </DireccionesWrapper>
        </ContactoWrapper>
        <WhatsappWrapper data-sal="slide-up" data-sal-easing="ease">
          <CardContacto licencias={licencias} />
        </WhatsappWrapper>
      </AgenciaWrapper>
    </Container>
  )
}

export default Contacto
