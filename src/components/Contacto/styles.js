import styled from "styled-components"
import { colors } from "../../styles/components"
import { deviceMax } from "../../styles/index"

export const AgenciaWrapper = styled.div`
  display: flex;
  margin-top: 40px;
  position: relative;
  @media ${deviceMax.tablet} {
    flex-direction: column;
  }
`

export const ContactoWrapper = styled.div`
  flex: 1;
  text-align: left;
  color: ${colors.grisOscuro};
  margin-bottom: 20px;
  & h4 {
    font-size: 1.5rem;
    margin-bottom: 0.45rem;
  }
  & li {
    margin: 0;
  }
  & li > div {
    padding: 5px 0px;
    margin: 10px 0px;
  }
  & ul {
    margin: 0;
    margin-top: 20px;
    list-style-type: none;
  }
`
export const WhatsappWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
`
export const DireccionesWrapper = styled.div`
  margin-top: 20px;
  & svg {
    margin-right: 10px;
  }
`

export const CardContacto = styled.div`
  position: relative;
  box-shadow: 0px 2px 20px #221f1f33;
  border-radius: 28px;
  max-width: 457px;
  flex: 1;
  text-align: left;
  color: ${colors.grisOscuro};
  padding: 30px;
  padding-bottom: 40px;
  z-index: 1;
  background-color: white;
  & h4 {
    text-align: center;
    font-size: 1.7rem;
    margin-bottom: 0.45rem;
  }
  & p {
    margin-top: 2rem;
    font-weight: 300;
  }

  & label {
    font-weight: bold;
    width: 100%;
    display: block;
    margin-top: 1rem;
    font-size: 0.9rem;
  }

  & input, select {
    display: block;
    width: 100%;
    background-color: white;
    border-radius: 1.5rem;
    border: 2px solid #a6bdd1;
    padding: 0px 10px;
    height: 2.1rem;
    font-size: 0.9rem;
  }
  & input:focus {
    outline: none;
    border: 2px solid ${colors.celeste};
  }

  & input:-internal-autofill-selected {
    background-color: white;
  }

  & select:focus {
    outline: none;
  }
`

export const BotonWrapper = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  right: 0;
  left: 0;
  bottom: -15px;
`

export const BotonCard = styled.button`
  background-color: ${colors.verde};
  border-radius: 16px;
  box-shadow: 0px 5px 20px #0000005d;
  color: white;
  text-align: center;
  border: 1px solid ${colors.verde};
  height: 40px;
  padding: 5px 15px;
  font-size: 0.85rem;
  transition: transform 0.3s ease;
  cursor: pointer;
  color: white;
  font-weight: bold;
  font-size: 0.9rem;
  opacity: ${props => props.disabled ? 0.5 : 1};
  &:hover {
    transform: scale(1.05);
  }
  &:active {
    transform: scale(0.98);
  }

  & svg {
    margin-left: 10px;
  }
`
export const DotsContacto = styled.div`
  position: absolute;
  width: 50vw;
  right: 0;
  top: 50px;
  border-top-left-radius: 60px;
  border-bottom-left-radius: 60px;
  overflow: hidden;
  z-index: 0;
`
