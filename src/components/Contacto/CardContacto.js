import React, { useEffect, useState, Fragment } from "react"
import { CardContacto, BotonCard, BotonWrapper } from "./styles"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { useForm } from "../../hooks/useForm"

const RUTA_BASE = "http://209.151.152.130/api/"
// const RUTA_BASE = 'http://192.168.0.161:8000/api/'

const CardContactoComponent = ({ licencias = false }) => {
  console.log("ES de licencias ", licencias)
  const [servicios, setServicios] = useState([])
  const [loader, setLoader] = useState(false)
  const [formValues, handleInputChange] = useForm({
    servicio: null,
    nombre: null,
    telefono: null,
  })
  const { servicio, nombre, telefono } = formValues
  const getServicios = async () => {
    const ruta = licencias
      ? `${RUTA_BASE}servicio/getServiciosAll/?es_tramite=true`
      : `${RUTA_BASE}servicio/getServiciosAll/?es_tramite=false`
    const reponse = await fetch(ruta)
    const data = await reponse.json()
    setServicios(data)
  }
  useEffect(() => {
    getServicios()
  }, [])
  const handleSubmit = async event => {
    event.preventDefault()
    console.log(" valores ", servicio, nombre, telefono)
    try {
      setLoader(true)
      await fetch(`${RUTA_BASE}prospecto/agregarProspectoLanding/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ servicio, nombre, telefono }),
      })
      const servicioSeleccionado = servicios.find(
        x => x.id === Number(servicio)
      )
      window.open(
        `https://api.whatsapp.com/send/?phone=+50254031403&text=Hola+soy+${nombre},+necesito+información+sobre+${servicioSeleccionado.nombre}&app_absent=0`,
        "_blank"
      )
    } catch (error) {
      alert("No se logró conectar con el servidor. Intente más tarde")
    }
    setLoader(false)
  }
  return (
    <CardContacto>
      {licencias ? (
        <Fragment>
          <h4>¿Qué requisitos necesito?</h4>
          <p>
            Obtén la información completa para tu caso en específico por
            WhatsApp, sabrás la disponibilidad de citas y el precio completo de
            tu licencia.
          </p>
        </Fragment>
      ) : (
        <Fragment>
          <h4>¿Qué curso es ideal para mí?</h4>
          <p>
            Obtén un test gratuito por WhatsApp, sabrás el tiempo aproximado de
            horas que necesitas y el precio del curso.
          </p>
        </Fragment>
      )}

      <form>
        <div>
          <label htmlFor="nombre">Nombre</label>
          <input
            id="nombre"
            name="nombre"
            type="text"
            placeholder="Alberto Rodríguez"
            value={nombre}
            onChange={handleInputChange}
            required
          />
        </div>
        <div>
          <label htmlFor="telefono">Teléfono</label>
          <input
            id="telefono"
            name="telefono"
            type="number"
            placeholder="58691264"
            value={telefono}
            onChange={handleInputChange}
            required
          />
        </div>
        <div>
          <label htmlFor="servicio">Servicio</label>
          <select
            id="servicio"
            name="servicio"
            value={servicio}
            placeholder="Seleccione servicio"
            onChange={handleInputChange}
            required
          >
            <option value="" disabled selected>
              Seleccione un servicio
            </option>
            {servicios.map(servicio => {
              return (
                <option key={servicio.id} value={`${servicio.id}`}>
                  {servicio.nombre}
                </option>
              )
            })}
          </select>
        </div>

        <BotonWrapper>
          <BotonCard
            onClick={handleSubmit}
            aria-label="Obtener tests"
            disabled={loader}
          >
            {loader ? (
              <Fragment>ENVIANDO...</Fragment>
            ) : (
              <Fragment>
                OBTENER TEST GRATIS
                <FontAwesomeIcon icon={faWhatsapp} />
              </Fragment>
            )}
          </BotonCard>
        </BotonWrapper>
      </form>
    </CardContacto>
  )
}

CardContacto.propTypes = {}

export default CardContactoComponent
