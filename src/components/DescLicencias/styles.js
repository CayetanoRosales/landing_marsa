import styled from 'styled-components';

export const DotsWrapper = styled.div`
    position: absolute;
    width: 40vw;
    right: 0;
    top: -110px;
    border-top-left-radius: 60px;
    border-bottom-left-radius: 60px;
    overflow: hidden;
    z-index: 0;
`;