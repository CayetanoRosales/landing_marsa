import React, { useState } from "react"
import {
  DescripcionWrapper,
  ListadoWrapper,
  CheckDiv,
  ListaItem,
  CheckWrapper,
  DescripcionCuerpo,
  CarrouselWrapper,
  BodyCarrousel,
} from "../Descripcion/styles"
import { 
  ContainerVideo,
  VisorVideo,
} from '../../styles/components'
import { Container } from "../../styles/components"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faCheck,
} from "@fortawesome/free-solid-svg-icons"
import { DotsWrapper } from './styles'

const data = [
  "Lo más importante es tu seguridad.",
  "Capacitamos para tu vida, no para el examen.",
  "Rápidez y confianza.",
  "Nuestros evaluadores son pacientes.",
  "Somos los únicos con capacitación práctica antes del examen, dentro del circuito de evaluación.",
]

const Descripcion = () => {
  return (
    <DescripcionWrapper>
      <DotsWrapper>
        <div className="patter-dot" />
      </DotsWrapper>
      <Container>
        <DescripcionCuerpo>
          <ListadoWrapper data-sal="slide-up" data-sal-easing="ease">
            <h4>SIENTE EL ORGULLO DE <br /> ÓBTENER TU LICENCIA <br/> EN MARSA</h4>
            <ul>
              {data.map((item, index) => (
                <li key={`item-${index}`}>
                  <ListaItem>
                    <CheckWrapper>
                      <CheckDiv>
                        <FontAwesomeIcon icon={faCheck} />
                      </CheckDiv>
                    </CheckWrapper>
                    <div>{item}</div>
                  </ListaItem>
                </li>
              ))}
            </ul>
          </ListadoWrapper>
          <CarrouselWrapper>
            <BodyCarrousel>
              <ContainerVideo>
                <VisorVideo>
                  <iframe
                    iframe
                    title="Introducción a Marsa"
                    src="https://player.vimeo.com/video/73625505?badge=0&amp;byline=0&amp;title=0&amp;portrait=0"
                    width="100%"
                    height="290"
                    frameBorder="0"
                    webkitallowfullscreen
                    mozallowfullscreen
                    allowFullScreen
                  ></iframe>
                </VisorVideo>
              </ContainerVideo>
            </BodyCarrousel>
          </CarrouselWrapper>
        </DescripcionCuerpo>
      </Container>
    </DescripcionWrapper>
  )
}

export default Descripcion
