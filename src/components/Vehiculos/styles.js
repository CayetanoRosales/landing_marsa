import styled from "styled-components"

export const ImgVehiculo = styled.div`
  border-radius: 30px;
  width: 360px;
  max-height: 220px;
  margin: 20px;
  box-shadow: 0px 5px 20px #00000066;
  position: relative;

  overflow: hidden;
  height: calc(220 / 340 * 110%);
  picture > img {
    height: auto !important;
  }
  > img {
    height: auto !important;
  }
`
export const VehiculosWrapper = styled.section`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-top: 3rem;
`

export const DotsSuperior = styled.div`
  position: absolute;
  width: 35vw;
  right: 0;
  top: 100px;
  border-top-left-radius: 60px;
  border-bottom-left-radius: 60px;
  overflow: hidden;
`

export const DotsInferior = styled.div`
  position: absolute;
  width: 50vw;
  left: 0;
  bottom: 90px;
  border-top-right-radius: 60px;
  border-bottom-right-radius: 60px;
  overflow: hidden;
`
