import React from "react"
import { Container, TituloSeccionContainer } from "../../styles/components"
import VehiculoItem from "./Vehiculo"
import { VehiculosWrapper, DotsSuperior, DotsInferior } from "./styles"

const data = [
  {
    img: "moto.jpeg",
    alt: "moto",
  },
  {
    img: "carro.jpg",
    alt: "carro",
  },
  {
    img: "pickup.jpg",
    alt: "pickup",
  },
  {
    img: "camion.jpg",
    alt: "camion",
  },
  {
    img: "trailer.jpg",
    alt: "trailer",
  },
]
const Vehiculos = () => {
  return (
    <Container>
      <DotsSuperior>
        <div className="patter-dot" />
      </DotsSuperior>
      <DotsInferior>
        <div className="patter-dot" />
      </DotsInferior>
      <TituloSeccionContainer>
        <h4>ELIJE EL VEHÍCULO</h4>
        <p>Nosotros te enseñamos.</p>
      </TituloSeccionContainer>
      <VehiculosWrapper>
        {data.map(item => (
          <VehiculoItem key={item.alt} {...item} />
        ))}
      </VehiculosWrapper>
    </Container>
  )
}

export default Vehiculos
