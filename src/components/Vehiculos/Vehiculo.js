import React from "react"
import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import { ImgVehiculo } from "./styles"
export default ({ img, alt }) => {
  return (
    <StaticQuery
      query={graphql`
        query {
          allImageSharp {
            edges {
              node {
                fluid(maxHeight: 250) {
                  aspectRatio
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                  originalName
                }
              }
            }
          }
        }
      `}
      render={data => {
        const image = data.allImageSharp.edges.find(edge => {
          return edge.node.fluid.originalName === img
        })

        if (!image) {
          return null
        }

        return (
          <ImgVehiculo
            data-sal="zoom-in"
            data-sal-delay="200"
            data-sal-easing="ease"
          >
            <Img
              alt={alt}
              fluid={image.node.fluid}
              imgStyle={{
                height: "100%",
                width: "100%",
              }}
            />
          </ImgVehiculo>
        )
      }}
    />
  )
}
