/* eslint-disable react/jsx-no-target-blank */
import PropTypes from "prop-types"
import React, { useEffect, useState, Fragment } from "react"
import { Link } from "gatsby";
import Logo from "./logo"
import { Header, Button } from "../styles/components"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBars } from "@fortawesome/free-solid-svg-icons"
import { BotonesContainer, MenuHamburguesa } from "../styles/components"

const HeaderComponent = ({ transparente, setMenuOpen, ruta }) => {
  const [esHome, setEsHome] = useState(true);
  useEffect(() => {
    if (ruta === 'home') {
      setEsHome(true);
    } else if (ruta === 'licencias') {
      setEsHome(false);
    }
  }, []);
  
  useEffect(() => {
    // Create and append the Facebook domain verification meta tag
    const metaTag = document.createElement('meta');
    metaTag.setAttribute('name', 'facebook-domain-verification');
    metaTag.setAttribute('content', 'voliy2788bdt11jbeybmbam8vrl0vc');
    document.head.appendChild(metaTag);
  }, []);
  
  return (
    <Header
      style={{ background: `${transparente ? "transparent" : "#09133F"}` }}
    >
      <div>
        <Link to="/">
          <Logo />
        </Link>
      </div>
      <BotonesContainer>
        {
          (esHome) ? (
            <Fragment>
              <Link to="licencias">
                <Button aria-label="Licencias">LICENCIAS</Button>
              </Link>
              <a href="https://api.whatsapp.com/send/?phone=+50254031403&text=Hola,+necesito+informaci�n+para+inscribirme&app_absent=0" target="_blank">
                <Button primary aria-label="Inscribirme">
                  INSCRIBIRME
                </Button>
              </a>
            </Fragment>
            ) : (
            <Fragment>
              <a href="/">
                <Button
                 style={{ border: '2px solid', borderColor: `${transparente ? "#707071" : "#fff"}`, color: `${transparente ? "#707071" : "#fff"}` }}
                 aria-label="Licencias">
                  CURSOS
                </Button>
              </a>
              <a href="https://api.whatsapp.com/send/?phone=+50254031403&text=Hola,+necesito+una+cita+para+licencia&app_absent=0" target="_blank">
                <Button primary aria-label="reservar cita">
                  RESERVAR CITA
                </Button>
              </a>
            </Fragment>
          )
        }
        
      </BotonesContainer>
      <MenuHamburguesa>
        <FontAwesomeIcon
          onClick={() => setMenuOpen(true)}
          icon={faBars}
          size="2x"
        />
      </MenuHamburguesa>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-217527334-1">
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)}
  gtag('js', new Date());

  gtag('config', 'UA-217527334-1');
</script>
    </Header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default HeaderComponent
