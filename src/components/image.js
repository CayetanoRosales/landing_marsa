import React from "react"
import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"

export default ({ name }) => {
  return (
    <StaticQuery
      query={graphql`
        query {
          allImageSharp {
            edges {
              node {
                fluid(maxWidth: 500) {
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                  originalName
                }
              }
            }
          }
        }
      `}
      render={data => {
        const image = data.allImageSharp.edges.find(edge => {
          return edge.node.fluid.originalName === name
        })

        if (!image) {
          return null
        }

        return <Img fluid={image.node.fluid} />
      }}
    />
  )
}
