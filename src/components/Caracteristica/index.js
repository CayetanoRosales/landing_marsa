import React from "react"
import Card from "./Card"
import { Container, TituloSeccionContainer } from "../../styles/components"
import { TarjetasWrapper } from "./styles"
import {
  faShieldAlt,
  faMobileAlt,
  faMoneyBill,
} from "@fortawesome/free-solid-svg-icons"

const data = [
  {
    id: "seguridad",
    titulo: "Seguridad",
    contenido:
      "Siéntete libre en nuestra pista privada de entrenamiento, simuladores y con instructores altamente capacitados.",
    icono: faShieldAlt,
  },
  {
    id: "pago",
    titulo: "Facilidad de pago",
    contenido:
      "Conduce profesionalmente y a un precio increíble. Toma un curso completo desde Q169 al mes.",
    icono: faMoneyBill,
  },
  {
    id: "cursos",
    titulo: "Cursos en línea",
    contenido: "Aprende desde la comodidad de tu hogar y a tu propio ritmo.",
    icono: faMobileAlt,
    ruta: "https://react-slick.neostack.com/",
  },
]

export default () => {
  return (
    <Container>
      <TituloSeccionContainer>
        <h4>LLEGA A LA META</h4>
        <p>Cada persona es única, por eso nuestro método se adapta a tí.</p>
      </TituloSeccionContainer>
      <TarjetasWrapper>
        {data.map(item => (
          <Card key={item.id} {...item} />
        ))}
      </TarjetasWrapper>
    </Container>
  )
}
