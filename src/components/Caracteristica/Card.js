import React from "react"
import {
  CardWrapper,
  IconoWrapper,
  ContenedorWrapper,
  BotonCard,
  BotonWrapper,
} from "./styles"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const Card = ({ titulo, contenido, icono, ruta }) => {
  return (
    <CardWrapper data-sal="slide-up" data-sal-easing="ease">
      <ContenedorWrapper>
        <IconoWrapper>
          <FontAwesomeIcon icon={icono} />
        </IconoWrapper>
      </ContenedorWrapper>
      <h5>{titulo}</h5>
      <p>{contenido}</p>
      {ruta && (
        <BotonWrapper>
          <a href={ruta}>
            <BotonCard aria-label="Cursos virtuales">
              Ver cursos virtuales
            </BotonCard>
          </a>
        </BotonWrapper>
      )}
    </CardWrapper>
  )
}

export default Card
