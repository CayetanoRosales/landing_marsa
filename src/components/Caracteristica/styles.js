import styled from "styled-components"
import { colors } from "../../styles/components"

export const TarjetasWrapper = styled.div`
  margin-top: 50px;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`
export const ContenedorWrapper = styled.div`
  height: 100px;
  display: flex;
  justify-content: center;
`

export const CardWrapper = styled.div`
  box-shadow: 0px 2px 20px #221f1f33;
  border-radius: 28px;
  padding: 30px;
  padding-top: 55px;
  color: ${colors.grisOscuro};
  display: flex;
  position: relative;
  flex-direction: column;
  height: 380px;
  width: 290px;
  margin: 15px;
  & h5 {
    text-align: center;
    font-size: 1.4rem;
  }
  & p {
    text-align: center;
    font-size: 0.9rem;
    line-height: 1.2;
  }
`

export const IconoWrapper = styled.div`
  background: ${colors.amarillo};
  transform: rotateZ(45deg);
  height: 70px;
  width: 70px;
  border-radius: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 25px;

  & svg {
    transform: rotateZ(-45deg);
  }
`
export const BotonCard = styled.button`
  background-color: ${colors.celeste};
  border-radius: 16px;
  box-shadow: 0px 5px 20px #0000005d;
  color: white;
  text-align: center;
  border: 1px solid ${colors.celeste};
  height: 30px;
  padding: 0px 15px;
  font-size: 0.95rem;
  transition: transform 0.3s ease;
  cursor: pointer;
  &:hover {
    transform: scale(1.05);
  }
  &:active {
    transform: scale(0.98);
  }
`
export const BotonWrapper = styled.div`
  width: 100%;
  position: absolute;
  right: 0;
  left: 0;
  bottom: -15px;
  display: flex;
  justify-content: center;
`
