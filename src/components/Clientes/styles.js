import styled from "styled-components"
import { colors } from "../../styles/components"
import { deviceMax } from "../../styles"

export const ClientesWrapper = styled.div`
  position: relative;
  background: ${colors.azul};
`
export const TituloWrapper = styled.div`
  text-align: center;
  color: white;
  & h4 {
    font-size: 1.5rem;
    margin-bottom: 0.45rem;
  }
`

export const ClienteCard = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  color: white;
  position: relative;
  height: 350px;
`

export const BodyCard = styled.div`
  position: relative;
  border: 2px solid #fff;
  border-radius: 20px;
  padding: 30px;
  text-align: center;
  padding-top: 60px;
  height: 275px;
  margin: 0px 10px;
  & h5 {
    margin: 0px;
  }
`

export const ComiaIzquierda = styled.div`
  font-size: 70px;
  color: #fff;
  position: absolute;
  top: -14px;
  left: 0;
`

export const ComiaDerecha = styled.div`
  font-size: 70px;
  color: #fff;
  position: absolute;
  bottom: 0;
  right: 0;
`

export const ImagenCliente = styled.div`
  border: 2px solid #fff;
  border-radius: 100%;
  height: 80px;
  width: 80px;
  & img {
    border-radius: 100%;
  }
`

export const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  top: -40px;
  right: 0;
  left: 0;
  z-index: 2;
`
export const DescripcionWrapper = styled.div`
  position: relative;
  margin-top: 20px;
  & p {
    padding: 0px 30px;
  }
`

export const SeccionClienteWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 30px;
  padding-top: 30px;
  padding-bottom: 30px;
  @media ${deviceMax.tablet} {
    grid-template-columns: 1fr;
  }
`
export const BotonCard = styled.button`
  background-color: ${colors.celeste};
  border-radius: 16px;
  box-shadow: 0px 5px 20px #0000005d;
  color: white;
  text-align: center;
  border: 1px solid ${colors.celeste};
  height: 30px;
  padding: 0px 15px;
  font-size: 0.95rem;
  transition: transform 0.3s ease;
  cursor: pointer;
  &:hover {
    transform: scale(1.05);
  }
  &:active {
    transform: scale(0.98);
  }
`
export const TestimoniosBotonWrapper = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  bottom: -15px;
  right: 0;
  left: 0;
`
