import React from "react"
import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"

import {
  ClienteCard,
  BodyCard,
  ImagenCliente,
  ImageWrapper,
  ComiaIzquierda,
  ComiaDerecha,
  DescripcionWrapper,
} from "./styles"
const Cliente = ({ imagen, mensaje, nombre, lugar }) => {
  return (
    <StaticQuery
      query={graphql`
        query {
          allImageSharp {
            edges {
              node {
                fluid(maxHeight: 250) {
                  aspectRatio
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                  originalName
                }
                fixed(width: 25) {
                  aspectRatio
                  ...GatsbyImageSharpFixed_withWebp_noBase64
                  originalName
                }
              }
            }
          }
        }
      `}
      render={data => {
        const image = data.allImageSharp.edges.find(edge => {
          return edge.node.fluid.originalName === imagen
        })
        const comillaIzquierda = data.allImageSharp.edges.find(edge => {
          return edge.node.fixed.originalName === "comillaIzquierda.png"
        })
        const comillaDerecha = data.allImageSharp.edges.find(edge => {
          return edge.node.fixed.originalName === "comillaDerecha.png"
        })

        if (!image) {
          return null
        }

        return (
          <ClienteCard>
            <BodyCard>
              <ImageWrapper>
                <ImagenCliente>
                  <Img
                    fluid={image.node.fluid}
                    imgStyle={{
                      height: "100%",
                      width: "100%",
                    }}
                  />
                </ImagenCliente>
              </ImageWrapper>
              <h5>{nombre}</h5>
              <small>{lugar}</small>
              <DescripcionWrapper>
                <ComiaIzquierda>
                  <Img fixed={comillaIzquierda.node.fixed} />
                </ComiaIzquierda>
                <ComiaDerecha>
                  <Img fixed={comillaDerecha.node.fixed} />
                </ComiaDerecha>
                <p>{mensaje}</p>
              </DescripcionWrapper>
            </BodyCard>
          </ClienteCard>
        )
      }}
    />
  )
}

export default Cliente
