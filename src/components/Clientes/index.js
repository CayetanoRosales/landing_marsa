/* eslint-disable react/jsx-no-target-blank */
import React from "react"
import {
  ClientesWrapper,
  TituloWrapper,
  SeccionClienteWrapper,
  BotonCard,
  TestimoniosBotonWrapper,
} from "./styles"
import { Link } from "gatsby"
import { Container } from "../../styles/components"
import ClienteCard from "./Cliente"
import "react-responsive-carousel/lib/styles/carousel.min.css" // requires a loader
import { Carousel } from "react-responsive-carousel"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFacebookF } from "@fortawesome/free-brands-svg-icons"

const data = [
  {
    item1: {
      imagen: "testimonial-1.png",
      mensaje:
        "¡Gracias! Los invito a todos para que sigan su sueño y no se queden atras.",
      nombre: "Erick Barrondo",
      lugar: "Guatemala",
    },
    item2: {
      imagen: "testimonial-2.png",
      mensaje:
        "¡La mejor del mundo! Marsa tiene un mejor método que las escuelas de Boston.",
      nombre: "Margalit Shapiro-Katz",
      lugar: "Boston",
    },
  },
  {
    item1: {
      imagen: "testimonial-3.png",
      mensaje:
        "Exelente pedagogía que me dió seguridad personal, responsabilidad en la conducción y mucho más. ¡Gracias!",
      nombre: "Mayra Moreira",
      lugar: "San José de Costa Rica",
    },
    item2: {
      imagen: "testimonial-4.png",
      mensaje:
        "A mi familia le gusta como conduzco, gracias a las técnicas de aprendizaje en Marsa.",
      nombre: "Lic. Claudia Pac Escalante",
      lugar: "Quetzaltenango",
    },
  },
  {
    item1: {
      imagen: "testimonial-5.png",
      mensaje:
        "¡Quiero volver! La seguridad que Marsa me dió, me hace querer conducir cosas más grandes.",
      nombre: "Cindy",
      lugar: "Huehuetenango",
    },
    item2: {
      imagen: "testimonial-6.png",
      mensaje:
        "¡Me ha cambiado vida! Puedo desplazarme a donde quiera y eso me ayuda en el trabajo.",
      nombre: "Dina",
      lugar: "Quetzaltenango",
    },
  },
]

const Cliente = () => {
  return (
    <ClientesWrapper>
      <Container>
        <TituloWrapper>
          <h4>FORMA PARTE DE LA FAMILIA MARSA</h4>
        </TituloWrapper>
        <Carousel
          showArrows={false}
          showStatus={false}
          infiniteLoop
          autoPlay
          showThumbs={false}
        >
          {data.map((item, index) => (
            <div key={`item-${index}`}>
              <SeccionClienteWrapper>
                <ClienteCard {...item.item1} />
                <ClienteCard {...item.item2} />
              </SeccionClienteWrapper>
            </div>
          ))}
        </Carousel>
      </Container>
      <TestimoniosBotonWrapper>
        <a href="https://www.facebook.com/escuelamarsa92" target="_blank">
          <BotonCard aria-label="Ver testimonios">
            Ver más testimonios en <FontAwesomeIcon icon={faFacebookF} />{" "}
          </BotonCard>
        </a>
      </TestimoniosBotonWrapper>
    </ClientesWrapper>
  )
}

export default Cliente
