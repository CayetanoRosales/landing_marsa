import React from "react"
import Logo from "./logo"
import { Footer, IconosFooter, DerechosFooter } from "../styles/components"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faFacebookF,
  faInstagram,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons"

const FooterComponent = () => {
  return (
    <Footer>
      <Logo />
      <IconosFooter>
        <a href="https://www.facebook.com/escuelamarsa92">
          <FontAwesomeIcon icon={faFacebookF} />
        </a>
        <a href="https://www.instagram.com/marsaescuela/">
          <FontAwesomeIcon icon={faInstagram} />
        </a>
        <a href="https://www.youtube.com/">
          <FontAwesomeIcon icon={faYoutube} />
        </a>
      </IconosFooter>
      <DerechosFooter>© MARSA - {new Date().getFullYear()}</DerechosFooter>
    </Footer>
  )
}

export default FooterComponent
