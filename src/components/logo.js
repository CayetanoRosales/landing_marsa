import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import Img from "gatsby-image"
import { LogoWrapper } from "../styles/components"
const Logo = () => {
  const data = useStaticQuery(
    graphql`
      query GET_IMAGE {
        file(relativePath: { eq: "logo.png" }) {
          childImageSharp {
            fixed(height: 55) {
              ...GatsbyImageSharpFixed_withWebp_noBase64
            }
          }
        }
      }
    `
  )
  return (
    <LogoWrapper>
      <Img fixed={data.file.childImageSharp.fixed} />
    </LogoWrapper>
  )
}

export default Logo
