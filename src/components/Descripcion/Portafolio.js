import React from "react"
import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
export default ({ img }) => {
  return (
    <StaticQuery
      query={graphql`
        query {
          allImageSharp {
            edges {
              node {
                fluid(maxHeight: 500) {
                  aspectRatio
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                  originalName
                }
              }
            }
          }
        }
      `}
      render={data => {
        const image = data.allImageSharp.edges.find(edge => {
          return edge.node.fluid.originalName === img
        })

        if (!image) {
          return null
        }

        return (
          <Img
            fluid={image.node.fluid}
            imgStyle={{
              height: "100%",
              width: "100%",
            }}
          />
        )
      }}
    />
  )
}
