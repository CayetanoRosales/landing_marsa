import React, { useState } from "react"
import { Link } from "gatsby"
import {
  DescripcionWrapper,
  ListadoWrapper,
  CheckDiv,
  ListaItem,
  CheckWrapper,
  BotonCard,
  DescripcionCuerpo,
  CarrouselWrapper,
  CardImagen,
  BotonesWrapper,
  BodyCarrousel,
  BotonNavegar,
  CuerpoBotones,
} from "./styles"
import { Container } from "../../styles/components"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faCheck,
  faArrowLeft,
  faArrowRight,
} from "@fortawesome/free-solid-svg-icons"
import { Carousel } from "react-responsive-carousel"
import Portafolio from "./Portafolio"

const data = [
  "Enseñamos desde 1992.",
  "Instructores con paciencia.",
  "Horarios flexibles.",
  "Pista privada.",
  "Simulador de conducción.",
  "Seminario de seguridad vial.",
  "Capacitación para licencia.",
  "App de alumnos.",
]
const imagenes = [
  "portfolio-1.jpg",
  "portfolio-2.jpg",
  "portfolio-3.jpg",
  "portfolio-4.jpg",
  "portfolio-5.jpg",
  "portfolio-6.jpg",
  "portfolio-7.jpg",
  "portfolio-8.jpg",
  "portfolio-9.jpg",
  "portfolio-10.jpg",
  "portfolio-11.jpg",
  "portfolio-12.jpg",
]
const Descripcion = () => {
  const [currentSlide, setCurrentSlide] = useState(0)

  const next = () => {
    setCurrentSlide(currentSlide + 1)
  }
  const prev = () => {
    setCurrentSlide(currentSlide - 1)
  }
  const updateCurrentSlide = index => {
    if (currentSlide !== index) {
      setCurrentSlide(index)
    }
  }
  return (
    <DescripcionWrapper>
      <div className="dots-descripcion">
        <div className="patter-dot" />
      </div>
      <Container>
        <DescripcionCuerpo>
          <ListadoWrapper data-sal="slide-up" data-sal-easing="ease">
            <h4>CONDUCIR TE ENCANTARÁ</h4>
            <ul>
              {data.map((item, index) => (
                <li key={`item-${index}`}>
                  <ListaItem>
                    <CheckWrapper>
                      <CheckDiv>
                        <FontAwesomeIcon icon={faCheck} />
                      </CheckDiv>
                    </CheckWrapper>
                    <div>{item}</div>
                  </ListaItem>
                </li>
              ))}
            </ul>
            <a href="https://react-slick.neostack.com/">
              <BotonCard>INSCRIBIRME</BotonCard>
            </a>
          </ListadoWrapper>
          <CarrouselWrapper>
            <BodyCarrousel>
              <CardImagen>
                <Carousel
                  selectedItem={currentSlide}
                  onChange={updateCurrentSlide}
                  showArrows={false}
                  showStatus={false}
                  infiniteLoop
                  autoPlay
                  showThumbs={false}
                  showIndicators={false}
                >
                  {imagenes.map((item, index) => (
                    <div key={`item-${index}`}>
                      <Portafolio img={item} />
                    </div>
                  ))}
                </Carousel>
              </CardImagen>
              <CuerpoBotones>
                <BotonesWrapper>
                  <BotonNavegar aria-label="Anterior" onClick={prev}>
                    <FontAwesomeIcon icon={faArrowLeft} />
                  </BotonNavegar>
                  <BotonNavegar onClick={next} aria-label="Siguiente">
                    <FontAwesomeIcon icon={faArrowRight} />
                  </BotonNavegar>
                </BotonesWrapper>
              </CuerpoBotones>
            </BodyCarrousel>
          </CarrouselWrapper>
        </DescripcionCuerpo>
      </Container>
    </DescripcionWrapper>
  )
}

export default Descripcion
