import styled from "styled-components"
import { colors } from "../../styles/components"
import { deviceMax } from "../../styles"

export const DescripcionWrapper = styled.div`
  position: relative;
  background: #eaeff5;
`

export const DescripcionCuerpo = styled.div`
  display: flex;
  @media ${deviceMax.tablet} {
    flex-direction: column;
  }
`

export const BotonCard = styled.button`
  background-color: ${colors.celeste};
  border-radius: 16px;
  box-shadow: 0px 5px 20px #0000005d;
  color: white;
  text-align: center;
  border: 1px solid ${colors.celeste};
  height: 30px;
  padding: 0px 15px;
  font-size: 0.8rem;
  transition: transform 0.3s ease;
  cursor: pointer;
  &:hover {
    transform: scale(1.05);
  }
  &:active {
    transform: scale(0.98);
  }
`
export const CarrouselWrapper = styled.div`
  flex: 1;
  position: relative;
  margin-bottom: 40px;
`
export const BodyCarrousel = styled.div`
  position: relative;
`
export const ListadoWrapper = styled.div`
  flex: 1;
  color: ${colors.grisOscuro};
  margin-bottom: 40px;
  & h4 {
    font-size: 1.5rem;
    margin-bottom: 0.45rem;
    line-height: 2.2rem; 
  }
  & p {
    font-size: 0.9rem;
    line-height: 1.2;
  }

  & ul {
    margin: 0;
    margin-top: 40px;
    margin-bottom: 40px;
    list-style-type: none;
  }
`
export const CheckDiv = styled.div`
  border: 2px solid ${colors.celeste};
  border-radius: 100%;
  width: 22px;
  height: 22px;
  font-size: 12px;
  color: ${colors.celeste};
  display: flex;
  justify-content: center;
  align-items: center;
`

export const CheckWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const ListaItem = styled.div`
  display: grid;
  grid-template-columns: 30px 1fr;
  grid-column-gap: 10px;
`
export const CardImagen = styled.div`
  border-radius: 50px;
  box-shadow: -5px 5px 20px #00000067;
  overflow: hidden;
  position: relative;
  z-index: 3;
`
export const BotonesWrapper = styled.div`
  border-radius: 20px;
  background-color: rgba(112, 112, 107, 0.2);
  height: 150px;
  width: 300px;

  padding: 10px;
  display: flex;
  justify-content: start;
  align-items: flex-end;
  @media ${deviceMax.tablet} {
    justify-content: center;
  }
`
export const CuerpoBotones = styled.div`
  position: absolute;
  bottom: -80px;
  left: -80px;
  right: 0;
  z-index: 0;
  display: flex;
  justify-content: flex-start;
  @media ${deviceMax.tablet} {
    left: 10px;
    right: 10px;
    justify-content: center;
  }
`

export const BotonNavegar = styled.button`
  border-radius: 100%;
  border: 1px solid ${colors.celeste};
  color: white;
  height: 50px;
  width: 50px;
  margin: 0px 10px;
  background-color: ${colors.celeste};
  cursor: pointer;
`
