import { css, createGlobalStyle } from "styled-components"

export const size = {
  small: 400,
  medium: 480,
  mediumL: 960,
  large: 1140,
}

export const above = Object.keys(size).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${size[label]}px) {
      ${css(...args)}
    }
  `
  return acc
}, {})

export const GlobalStyles = createGlobalStyle`
  .___gatsby{
    position: absolute;
    padding: 0;
    margin: 0;
  }
`

export const sizeResponsive = {
  mobileS: "320px",
  mobileM: "375px",
  mobileL: "425px",
  tablet: "768px",
  laptop: "1024px",
  laptopL: "1440px",
  desktop: "2560px",
}

export const deviceMax = {
  mobileS: `(max-width: ${sizeResponsive.mobileS})`,
  mobileM: `(max-width: ${sizeResponsive.mobileM})`,
  mobileL: `(max-width: ${sizeResponsive.mobileL})`,
  tablet: `(max-width: ${sizeResponsive.tablet})`,
  laptop: `(max-width: ${sizeResponsive.laptop})`,
  laptopL: `(max-width: ${sizeResponsive.laptopL})`,
  desktop: `(max-width: ${sizeResponsive.desktop})`,
  desktopL: `(max-width: ${sizeResponsive.desktop})`,
}
