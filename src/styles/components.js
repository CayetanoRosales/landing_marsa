import styled from "styled-components"
import { deviceMax } from "./index"
const IMAGEN = require("../images/fondo1.jpg")

export const colors = {
  azul: "#09133F",
  celeste: "#1E90FF",
  verde: "#13BA35",
  grisClaro: "#EAEFF5",
  grisOscuro: "#5D5D5D",
  amarillo: "#F7C700",
}

export const LogoWrapper = styled.div`
  display: flex;
  height: 55px;
  @media ${deviceMax.mobileL} {
    justify-content: center;
  }
`
export const Header = styled.header`
  position: fixed;
  right: 0;
  top: 0;
  left: 0;
  z-index: 3;
  padding: 5px 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  z-index: 200;
  transition: all 0.3s ease-in-out;
  height: 70px;
`

export const Footer = styled.div`
  padding: 25px 20px;
  border-top: 1px solid ${colors.grisClaro};
  display: flex;
  justify-content: space-between;
  @media ${deviceMax.mobileL} {
    flex-direction: column;
  }
`

export const IconosFooter = styled.div`
  color: ${colors.grisOscuro};
  font-size: 24px;
  display: flex;
  align-items: center;
  & a {
    color: ${colors.grisOscuro};
  }
  & svg {
    margin: 0px 10px;
  }
  @media ${deviceMax.mobileL} {
    justify-content: center;
    margin: 10px 0px;
  }
`

export const DerechosFooter = styled.div`
  color: ${colors.grisOscuro};
  font-size: 16px;
  display: flex;
  align-items: center;
  @media ${deviceMax.mobileL} {
    justify-content: center;
  }
`

export const Container = styled.section`
  padding: 70px 10%;
  padding-bottom: 100px;
  position: relative;
`

export const Button = styled.button`
  /* Adapt the colors based on primary prop */
  background: ${props => (props.primary ? colors.celeste : "transparent")};
  color: white;
  font-size: 0.9em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid;
  height: 38px;
  width: 200px;
  border-color: ${props => (props.primary ? colors.celeste : "white")};
  border-radius: 16px;
  transition: all 0.3s ease-in-out;
  &:hover {
    cursor: pointer;
    transform: scale(1.1);
  }
  &:active {
    transform: scale(0.96);
  }
`
export const BannerComponent = styled.div`
  background: url(${IMAGEN});
  position: relative;
  height: 80vh;
  width: 100%;

  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

  &:before {
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    background-color: #131f5899;
  }
  @media ${deviceMax.tablet} {
    height: auto;
  }
`

export const BotonesContainer = styled.div`
  display: block;
  @media ${deviceMax.tablet} {
    display: none;
  }
`

export const MenuHamburguesa = styled.div`
  color: white;
  display: none;
  @media ${deviceMax.tablet} {
    display: block;
    display: flex;
    align-items: center;
  }
`

export const ContainerBanner = styled.div`
  display: grid;
  grid-template-columns: minmax(500px, 1fr) 1fr;
  position: relative;
  height: 100%;
  padding: 10px 10%;
  & h1 {
    color: white;
    font-size: 3.5rem;
  }
  & p {
    font-weight: 400;
    color: white;
    font-size: 1.75rem;
    line-height: 1.2;
  }
  @media ${deviceMax.tablet} {
    grid-template-columns: 1fr;
    padding-bottom: 15px;

    & h1,
    p {
      text-align: center;
    }
    & h1 {
      font-size: 2.5rem;
    }
    & p {
      font-size: 1rem;
    }
  }
  @media ${deviceMax.laptop} {
    padding: 10px 5%;
  }
  @media ${deviceMax.mobileL} {
    padding: 10px 10px;
  }
`

export const VisorVideo = styled.div`
  border: 3px solid white;
  border-radius: 20px;
  background: rgba(0, 0, 0, 0.3);
  display: flex;
  width: 518px;
  justify-content: center;
  align-items: center;
  & iframe {
    border-radius: 20px;
    margin: 0;
  }
  @media (max-width: 1330px) {
    width: 100%;
    flex: 1;
  }
`

export const ContainerTitulo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media ${deviceMax.tablet} {
    margin-top: 75px;
  }
`

export const ContainerVideo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

export const TituloSeccionContainer = styled.div`
  text-align: center;
  color: ${colors.grisOscuro};
  & h4 {
    font-size: 1.5rem;
    margin-bottom: 0.45rem;
  }
`

export const BotonMessenger = styled.div`
  border-radius: 100%;
  height: 80px;
  width: 80px;
  background-color: white;
  position: fixed;
  right: 20px;
  bottom: 20px;
  z-index: 10;
  box-shadow: 0px 5px 30px #221f1f33;
  display: flex;
  justify-content: center;
  align-items: center;
`
